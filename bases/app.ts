const msg = "Hola Mundo";

console.log(msg);
// En constantes debo especificar el tipo de variable
// let msg = "Hola Mundo";
// Despues de definirse una variable en string no recibe otro tipo de datos
// msg = 123